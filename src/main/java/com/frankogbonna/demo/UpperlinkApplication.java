package com.frankogbonna.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UpperlinkApplication {

    public static void main(String[] args) {
        SpringApplication.run(UpperlinkApplication.class, args);
    }

}
